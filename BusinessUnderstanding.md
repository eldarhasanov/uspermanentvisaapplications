### Business Understanding

1)__**Identifying the business goals**__

1.1)__**Background**__
	
Working and living in USA is a great opportunity for people to survive nowadays. 
In order to start working there, people must follow some procedures. 
Helping these people during their process is a great way of business so that 
they will spend less time on the application and be aware of precise conditions for the visa.
	
1.2)__**Business goals**__

In our business, we aim to assist those who need to apply for working visa in USA. 
Additionally, we would like to visualize the main criterions so that 
further generations can prepare themselves in order to start a life in USA. 
In other words, our goal is to increase the probability of successful visa applications 
and to make young people be aware of the visa procedure and working in USA. 

1.3)__**Business success criteria**__
	
Our success criteria is to look the visa application results of next 10 people 
who benefited our service and to observe at least 9 applications are approved.
	
2)__**Assessing the situation**__

2.1)__**Inventory of resources**__
	
**Data Miner:** DENIZALP KAPISIZ  contact: denizalp1634@gmail.com  
	
**Data Miner:** ELDAR HASANOV   contact: mr.eldarhasanov@gmail.com  
		
**Advisor:** MEELIS KULL  contact: meelis.kull@ut.ee  
	
**Data:** https://www.kaggle.com/jboysen/us-perm-visas  
	
**Development Environment:** RStudio link: https://www.rstudio.com

2.2)__**Requirements, assumptions, and constraints**__

The requirements for the acceptable work are the poster describing the project, 
the repository including the software of the project, and the kernel uploaded to Kaggle. 

Our schedule is organized so that 50 hours will be assigned for the project and 
each data miner will take 25 hours work totally. Below is our schedule:

**Start Date:** 25.12.2017  
  
**End Date:** 08.01.2018

In total, we have 15-days schedule for this project. 
Each miner will allocate at least 2 hours per day; however, 
allocations can be delayed to further dates, for example if two days are skipped, 
then at the third day at least 6 hours are allocated.
	
2.3)__**Risks and contingencies**__

**Risk 1:** RStudio is not working with the new update.

**Contingency plan 1:** Older version will be used for the development.

**Risk 2:** Bitbucket repository is not accessible due to some internal problems of the repository.

**Contingency plan 2:** Github repository will be created and the project will be migrated there.

**Risk 3:** The poster is not ready on 07.01.2018

**Contingency plan 3:** The poster will be ready on 08.01.2018

2.4)__**Terminology**__
	
**Data Understanding:** Analyzing each feature of data and deciding to which category it belongs. 

**Preprocessing:** Applying some operations on the dataset so that it becomes more understandable and is operated faster.

**Visualization:** Plotting the dataset with some features in order to understand the relationships.

**Schedule:** Time period denoting the life cycle of the project

**Analysis:** Interpreting the visualization such as understanding the relationships among the features and the top members in a feature.

**Prediction:** Guessing that the application will be approved or declined.

2.5)__**Costs and benefits**__

Since we will use free software for the development, this section is not applicable for our project.

3)__**Data mining goals**__

**Poster:** A paper to be pasted on the board that describes each section of the project, provides visualization, main ideas and references.

**Project Code:** The software that handles each subtask of the project.

**Kaggle Kernel:** The analysis of the dataset and project code provided in Kaggle in order for other people to understand the work.

4)__**Data-mining success criteria**__

The success criterion is to obtain at least 90% accuracy on the test data. 
The test data will be obtained from the dataset via randomization.