### Data Understanding

1.) __**Gathering data**__

1.1) __**Outline data requirements**__

Comma separated value(CSV) type is required as the data format.

1.2) __**Verify data availability**__

The required data to solve problem is indeed exist in dataset, which can be accessed by querying dataset using the columns below.

1.3) __**Define selection criteria**__

For this part we will use us_perm_visas.csv which consist of dataset for visa application results between 2012-17.
These are the selected attributes which will be used to determine our predicted results : 

application_type

class_of_admission

case_status

country_of_citizenship

employer_city

employer_country

employer_decl_info_title

employer_name

employer_num_employees

employer_state

foreign_worker_info_alt_edu_experience

foreign_worker_info_birth_country

foreign_worker_info_city

foreign_worker_info_education

foreign_worker_info_education_other

foreign_worker_info_inst

foreign_worker_info_major

foreign_worker_info_req_experience

foreign_worker_info_state

fw_info_yr_rel_edu_completed

job_info_work_city

job_info_experience

job_info_experience_num_months

job_info_title

job_info_work_state

pw_job_title_9089

2.) __**Describing data**__

As our data covers information about visa applications and their result for US, these above features hold information at below.

application_type : consist of types of visa applications 

class_of_admission :  consist of immigrant categories for US

case_status : the result of visa application

country_of_citizenship : consist of country name for foreign worker

employer_city :  consist of cityname for employer

employer_country :  consist of country name for employer

employer_decl_info_title :  consist of offered job name by employer

employer_name : consist of company names in US

employer_num_employees : consist of data for amount of employees in company

employer_state :  consist of state names in US

foreign_worker_info_alt_edu_experience :  consist of education levels

foreign_worker_info_birth_country :  consist of birth country name for foreign worker

foreign_worker_info_city :  consist of cityname for foreign worker

foreign_worker_info_education :  consist of higher education level for employee

foreign_worker_info_education_other : consist of other education levels for employee

foreign_worker_info_inst :  consist of institues names

foreign_worker_info_major : consist of major name for foreign employee

foreign_worker_info_req_experience : consist of value about requirement of experience

foreign_worker_info_state : consist of value for workers living state 

fw_info_yr_rel_edu_completed : consist of value for workers graduated education year

job_info_work_city :  consist of value for city of offered job

job_info_experience :  consist of info about requirement of experience

job_info_experience_num_months : consist of value about required month of experience

job_info_title : consist of value for offered job title





3.)__**Exploring data**__

application_type : MAILEDIN ONLINE PERM

case_status : Certified, Certified-Expired, Denied, Withdrawn

country_of_citizenship: Any country in world

employer_city : Any city in US

employer_decl_info_title : All available job types in employer company

employer_name : non-empty string value

employer_num_employees : integer greater than 1

employer_state : Any state in US

foreign_worker_info_alt_edu_experience  : A, N, Y

foreign_worker_info_birth_country : Any country in world

foreign_worker_info_city : Any city inside  foreign_worker_info_birth_country data

foreign_worker_info_education : Associate's Bachelor's Doctorate High School Master's None Other

foreign_worker_info_inst : All certified institutes

foreign_worker_info_major : Any kind of jobs

foreign_worker_info_req_experience : A, N, Y

foreign_worker_info_state :  Any state in US

job_info_work_city: Any state in US

job_info_experience: N, Y

job_info_experience_num_months:  integer between 0 - 200

job_info_title : Any available job title

job_info_work_state : Any state in US

pw_job_title_9089 : Any available job title


4.) __**Verifying data quality**__

The data is good enough to support our goals.


