### Planning

__**Detailed Plan: Each miner will allocate at least 2 hours per day; however, allocations can be delayed to further dates.**__

25.12.2017 => Project Kickoff - Understanding the Data

26.12.2017 => Data Preparation(Selecting and Cleaning data)

27.12.2017 => Data Preparation(Constructing and Formatting data)

28.12.2017 => Data Preparation(Visualization)

29.12.2017 => Data Preparation(Visualization)

30.12.2017 => Analysis

31.12.2017 => Analysis

01.01.2018 => Modeling(Selecting modeling techniquies)

02.01.2018 => Modeling(Building model, Assessing model)

03.01.2018 => Modeling(Building model, Assessing model)

04.01.2018 => Evaluation

05.01.2018 => Evaluation, Deployment

06.01.2018 => Poster 

07.01.2018 => Poster 

08.01.2018 => Presentation