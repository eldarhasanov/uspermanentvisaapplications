# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Kaggle Kernel :
	
https://www.kaggle.com/doorless/analysis-of-us-permanent-applications

### Direct link to the slide of US Permanent Visa Applications :
	
https://docs.google.com/presentation/d/1veA_WQcfRRx7hQnE8qklmsLYceWzQGrPHrieSEWqcaI/edit#slide=id.g2a4c3a4a7d_5_8

#### HOW TO RUN

In order to run the scripts, you must download the csv file from Kaggle and put this file 
under same directory with script files. You can find the link above the slide. Then,
you can run the scripts wherever is suitable for execution such as RStudio. There are some libraries required, 
we provided how you can resolve these issues if you open the script files with RStudio.  
  
### SCRIPTS  
There are three scripts; namely, cleaning.R, analysis.R, visualization.R and prediciton.R 

cleaning.R => Preparation of the data for the following operations

analysis.R => Analysis of the certified and denied decisions

visualization.R => Visualization of the applications according to some features

prediction.R => Prediction of new applications according to previous applications
